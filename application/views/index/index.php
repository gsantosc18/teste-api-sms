<?php $this->load->view('include/header') ?>

<div id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col s12 m6 l4">
				<div class="card-panel">
					<form action="https://www.aporfia.com.br/api_sms/" method="post" target="resultados">
						<legend class="flow-text">Dados da Instituição</legend>
						<div class="input-field">
							<input type="text" name="token" id="nome" class="validate">
							<label for="nome">Nome</label>
						</div>
						<div class="input-field">
							<input type="text" name="chave" class="validate">
							<label for="chave">Chave</label>
						</div>
						<div class="input-field">
							<input type="text" name="instituicao" id="Instituição" class="validate">
							<label for="Instituição">Instituicao</label>
						</div>
						<span class="flow-text">Informações do Cliente</span>
						<div class="input-field">
							<input type="text" name="celular" id="celular" class="validate">
							<label for="celular">celular</label>
						</div>
						<div class="input-field">
							<input type="text" name="mensagem" id="Mensagem" class="validate">
							<label for="Mensagem">Mensagem</label>
						</div>
						<div class="input-field">
							<input type="text" name="identificador" id="Identificador" class="validate">
							<label for="Identificador">Identificador</label>
						</div>
						<div class="input-field">
							<button type="submit" class="btn bue">Enviar</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col s12 m6 l8">
				<div class="card-panel row">
					<span class="flow-text">Resultados Retornados:</span>
					<iframe class="col s12" 
						name="resultados"
						frameborder="1">
					</iframe>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	iframe{
		border: 1px solid #ccc;
	}
</style>
<script>
	$( function() {
		window_size = $(window).height();
    	$('iframe').height(window_size-104);
	} );
</script>
<?php $this->load->view('include/footer') ?>
